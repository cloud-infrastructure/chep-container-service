\documentclass[a4paper]{jpconf}
\usepackage{graphicx}
\usepackage{url}
\usepackage{fancyvrb}
\usepackage{courier}
\usepackage{wrapfig}
\usepackage[dvipsnames]{xcolor}
\usepackage{listings}
\usepackage{minted}
\usepackage{booktabs}
\usepackage{varwidth}
\usepackage{capt-of}% or \usepackage{caption}

\newcommand\YAMLcolonstyle{\color{red}\mdseries}
\newcommand\YAMLkeystyle{\color{black}\bfseries}
\newcommand\YAMLvaluestyle{\color{blue}\mdseries}

\makeatletter

\newcommand\language@yaml{yaml}


\expandafter\expandafter\expandafter\lstdefinelanguage
\expandafter{\language@yaml}
{
  keywords={true,false,null,y,n},
  keywordstyle=\color{darkgray}\bfseries,
  basicstyle=\YAMLkeystyle,                                 % assuming a key comes first
  sensitive=false,
  comment=[l]{\#},
  morecomment=[s]{/*}{*/},
  commentstyle=\color{purple}\ttfamily,
  stringstyle=\YAMLvaluestyle\ttfamily,
  moredelim=[l][\color{orange}]{\&},
  moredelim=[l][\color{magenta}]{*},
  moredelim=**[il][\YAMLcolonstyle{:}\YAMLvaluestyle]{:},   % switch to value style at :
  morestring=[b]',
  morestring=[b]",
  literate =    {---}{{\ProcessThreeDashes}}3
                {>}{{\textcolor{red}\textgreater}}1     
                {|}{{\textcolor{red}\textbar}}1 
                {\ -\ }{{\mdseries\ -\ }}3,
}

% Define Language
\lstdefinelanguage{Docker}
{
  sensitive = false,
  keywords = [1]{from, run, expose, entrypoint,
  @get, @post, @put, @delete, require, ensure, otherwise, call},
  %ndkeywords={int, \char{}  },
  %ndkeywordstyle=\color{blue}\bfseries,
  basicstyle=\footnotesize\ttfamily,
  keywordstyle=\color{blue}\ttfamily,
  stringstyle=\color{red}\ttfamily,
  frame=trbl,
  morestring=[b]',
  morestring=[b]"
}

% switch to key style at EOL
\lst@AddToHook{EveryLine}{\ifx\lst@language\language@yaml\YAMLkeystyle\fi}
\makeatother

\newcommand\ProcessThreeDashes{\llap{\color{cyan}\mdseries-{-}-}}

\begin{document}
\title{Integrating Containers in the CERN Private Cloud}

\author{Bertrand Noel, Davide Michelino, Mathieu Velten, Ricardo Rocha and Spyridon Trigazis}
\address{European Organization for Nuclear Research (CERN), CH-1211 Geneva, Switzerland}

\begin{abstract}
Containers remain a hot topic in computing, with new use cases and tools appearing every day. Basic functionality such as spawning containers seems to have settled, but topics like volume support or networking are still evolving. Solutions like Docker Swarm, Kubernetes or Mesos provide similar functionality but target different use cases, exposing distinct interfaces and APIs.

The CERN private cloud is made of thousands of nodes and users, with many different use cases. A single solution for container deployment would not cover every one of them, and supporting multiple solutions involves repeating the same process multiple times for integration with authentication services, storage services or networking.

In this paper we describe OpenStack Magnum as the solution to offer container management in the CERN cloud. We will cover its main functionality and some advanced use cases using Docker Swarm and Kubernetes, highlighting some relevant differences between the two. We will describe the most common use cases in HEP and how we integrated popular services like CVMFS or AFS in the most transparent way possible, along with some limitations found. Finally we will look into ongoing work on advanced scheduling for both Swarm and Kubernetes, support for running batch like workloads and integration of container networking technologies with the CERN infrastructure.
\end{abstract}

\section{Introduction}

The CERN OpenStack cloud\cite{CERNBlog} entered production in 2013 and has since had a steady growth. As of the end of 2016 it hosts more than 2700 users across 3300 projects, with a total of 23000 VMs running on 7000 hypervisors. It includes a large range of OpenStack services, including the most commonly deployed: Keystone for authentication, authorization and service discovery; Glance for image storage and provision; Cinder for persistent block storage, backed by the CEPH storage system; Nova as the core of the system for virtual machine handling; and Horizon as the main user frontend dashboard. In addition we've more recently added: Heat as the orchestration service, for multi-node and more complex application stacks; Neutron as the new networking component, replacing the deprecated Nova Network.

The workloads being run by users vary from batch jobs doing simulation, event reconstruction and end user physics analysis and running infrastructure services on which the campus depends. For the majority of the long running services the deployment and configuration is typically managed by Puppet, but recently users started looking at containers as an alternative for improved flexibility and scalability.

Following this demand we started looking at how we could offer our users a container service that would be well integrated with the existing OpenStack infrastructure. Magnum appeared as a viable solution as it was compatible with our main goals:
\begin{itemize}
\item Integrate well with OpenStack at CERN, reusing our identity service, resource allocation, networking and data access
\item Support for multiple container orchestration services, especially Docker Swarm, Kubernetes and Mesos
\item Fast and easy to use, allowing users to scale clusters easily
\end{itemize}

In the following sections we give an overview of the reasons why containers are an exciting addition to our range of services, the technologies we chose and the process that lead us to a production service.

\section{Containers} \label{Containers}

Containers are a form of operating system level virtualization to run multiple isolated systems in the same host and kernel, avoiding a heavy virtualization layer to achieve increased performance and resource utilization. They exist in other platforms such as Solaris (called Solaris Zones) and in different flavors for Linux, including LXC (Linux containers), OpenVZ and more recently Docker.

To accomplish the required isolation between workloads and processes all of the solutions presented above rely on the following set of kernel features:
\begin{itemize}
\item cgroups, allowing limitation and prioritization of resources (CPU, memory, block I/O, network) without the need to start virtual machines
\item namespace isolation, giving individual applications independent views of the operating environment, including distinct process trees, networking, users and mounted filesystems
\end{itemize}

Running workloads on a shared kernel presents security challenges, and only after multiple iterations these issues have been mitigated. Users can run today multi-tenant workloads on a single machine with confidence, and add or remove sets of privileges as required.

\subsection{Docker}

Docker generalized container adoption by offering a greatly simplified interface to manage the concepts described above, as well as easy encapsulation of applications. Developers are able to fully describe their application setup in a simple manifest (a Dockerfile), which includes a series of commands and definitions. The result is a unit of deployment called a \textit{container image} that can be easily reused, deployed in heterogeneous environments and scaled during the lifetime of an application. Images can also be layered for space and resource usage optimization.

\begin{lstlisting}[language=Docker,caption=Dockerfile]
FROM        ubuntu:14.04
RUN         apt-get update && apt-get install -y redis-server
EXPOSE      6379
ENTRYPOINT  ["/usr/bin/redis-server"]
\end{lstlisting}

The resulting images are stored in registries (often shared publicly) and referenced by the application manifests used in container orchestration engines. The most popular registry is the Dockerhub, where official images for most popular services can be found. The simplicity of this process has made it popular initially in development environments and continuous integration systems, eventually making it all the way to production setups.

\section{Container Orchestration Engines} \label{OrchestrationEngines}
Among all types of tools already relying on the basic container building blocks, orchestration engines are the most relevant. These allow users to define their whole application stack in a single manifest file, including not only the actual application code to be run but also the networking, associated storage and relationships between the different pieces. This type of deployment is often called microservices due to its layout as a set of small coordinated services. Below we describe three of the most popular orchestration engines.

\subsection{Kubernetes} \label{Kubernetes}

Kubernetes is an open source project for managing containerized applications on top of an heteregoneous cluster of hosts. It provides basic but robust mechanisms to deploy, maintain, scale and update applications.

The base code has been contributed by Google and the project is now under the umbrella of the Cloud Native Computing Foundation, which is part of the Linux Foundation.

Some of its main abstractions include:
\begin{itemize}
\item Pod, a set of one or more co-scheduled containers, sharing network and local storage
\item ReplicaSet, ensures a specified number of replicas of a pod is running at any given time
\item Service, entrypoint and load balancer for client requests to a set of Pods
\item Deployment, a higher-level construct that manages a set of any other number of abstractions
\end{itemize}

There are many other concepts available, such as Secrets for secure declaration of data blobs,  StatefulSets for pods with special requirements regarding network identity or persistent storage, or Volumes for external block storage availability inside Pods.

Listings \ref{KubeService} and \ref{KubeDeployment} present manifests for a simple guestbook application:

\par\noindent
\begin{minipage}[t]{.45\textwidth}
\begin{lstlisting}[language=yaml, basicstyle=\footnotesize\ttfamily, breaklines=true, frame = single, label=KubeService, caption = Service]
apiVersion: v1
kind: Service
metadata:
  name: frontend
  labels:
    app: guestbook
    tier: frontend
spec:
  ports:
  - port: 80
  selector:
    app: guestbook
    tier: frontend
\end{lstlisting}
\end{minipage}%
\hfill
\begin{minipage}[t]{.45\textwidth}
\begin{lstlisting}[language=yaml, basicstyle=\footnotesize\ttfamily, breaklines=true, frame = single, label=KubeDeployment, caption = Deployment]
apiVersion: v1
kind: Deployment
metadata:
  name: frontend
spec:
  replicas: 3
  template:
    metadata:
      labels:
        app: guestbook
        tier: frontend
    spec:
      containers:
      - name: php-redis
        image: gcr.io/gb-frontend:v4
        resources:
          requests:
            cpu: 100m
            memory: 100Mi
        ports:
        - containerPort: 80
\end{lstlisting}%
\end{minipage}%

Internally Kubernetes is composed of several components:
\begin{itemize}
\item the Kubelet, runs on every node and interacts with the local container runtime (usually Docker) to comply with scheduler instructions
\item the API server, answers clients, maintains the global state of the cluster
\item the Scheduler, responsible for matching deployment requests to the current node status and availability
\item the Controller-Manager, responsible for matching the desired state with the current state of the cluster
\end{itemize}

\subsection{Docker Swarm} \label{Swarm}

Docker Swarm provides native clustering capabilities to turn a group of Docker engines into a single, virtual Docker instance. Initially a separate component, it is now part of the main Docker tool with different instances easily communicating together to form a single, huge computer.

Swarm provides similar abstractions to the ones described in Section \ref{Kubernetes} for Kubernetes, including Services, Volumes and Secrets. Services act as endpoints for clients to contact applications running in individual containers, making sure the load is properly balanced along the multiple replicas. The notion of a Pod does not exist, with each container having its own local storage and network stack. Integration with persistent storage is done via a clean and simple plugin API allowing vendors to provide their own volume plugins.

For orchestration Swarm relies on Docker Compose, which has recently also been integrated into the core Docker client. Deployments are called stacks and are defined using a yaml language similar to the one for Kubernetes manifests. Listings \ref{ComposeRedis} and \ref{ComposeDb} show an example deployment.

\par\noindent
\begin{minipage}[t]{.45\textwidth}
\begin{lstlisting}[language=yaml, basicstyle=\footnotesize\ttfamily, breaklines=true, frame = single, label=ComposeRedis, caption = Docker Compose]
version: "3"
services:
  redis:
    image: redis:alpine
    ports:
      - "6379"
    networks:
      - frontend
    deploy:
      replicas: 2
\end{lstlisting}%
\end{minipage}%
\hfill
\begin{minipage}[t]{.45\textwidth}
\begin{lstlisting}[language=yaml, basicstyle=\footnotesize\ttfamily, breaklines=true, frame = single, label=ComposeDb, caption = Docker Compose (continue)]
  db:
    image: postgres:9.4
    volumes:
      - db-data:/var/lib/postgresql/data
    networks:
      - backend
    deploy:
      placement:
        constraints: [node.role == manager]
\end{lstlisting}%
\end{minipage}%

\subsection{Mesos/DCOS} \label{Mesos}

Mesos\cite{Mesos} is an Apache top level project, describing itself as the \textit{datacenter kernel}. It abstracts datacenter resources (CPU, RAM, storage) and proposes them to registered frameworks, in the form of offers which they can accept or decline. Each framework can run one or more applications isolated via containers or pure cgroups.

Popular frameworks include Marathon for service deployment and Chronos to run scheduled tasks, with support for a very wide range of applications such as Spark, Hadoop or Elastic Search. One of its main features is the two level scheduling, allowing easy integration of containerized and cloud native with legacy applications via custom scheduling policies.

DCOS is a distribution built on top of Mesos initially released as a commercial product but recently made open source. It adds more advanced features such as L7 load balancing and advanced management tools, including a framework marketplace and web and command line interfaces.

\section{OpenStack Magnum} \label{Magnum}

Magnum is an API service developed by the OpenStack Containers team handling container cluster management. The three essencial concepts in Magnum are:
\begin{itemize}
\item Cluster Template, a reusable layout for cluster deployments
\item Cluster, a specific deployment of a container cluster
\item Node, which can be either a virtual machine or a baremetal server
\end{itemize}

Cluster templates contain the full description of how a cluster should look like, most importantly the container orchestration engine (COE), the type of node (VM or baremetal), the flavors to be used for both the master and the nodes, the server image (typically Fedora Atomic or CoreOS), and any advanced features like load balancing.

Currently there is support for Kubernetes, Docker Swarm, Apache Mesos and DC/OS, leaving users with a good set of options to match their use cases.

\subsection{Architecture}

Figure \ref{MagnumArchitecture} introduces the different interactions between Magnum, clients and other OpenStack components. Following the usual OpenStack service layout it consists of two daemons, the magnum-api and magnum-conductor. The first handles user requests for cluster templates, clusters and certificates, while the second handles all other asynchronous operations. Communication between the two is done via RPC queues over a RabbitMQ server.

To handle the heavy work of deploying and managing the multiple cluster components Magnum relies on OpenStack Heat, and each cluster is represented by a set of Heat stacks. Once a cluster is available the client can request a set of TLS certificates to communicate directly with the orchestration engine via its native API. Each cluster has a distinct certificate authority for credential isolation.

\begin{figure}[!h] \label{MagnumArchitecture}
\includegraphics[width=1.0\textwidth]{magnum-architecture}
\caption{Magnum Architecture}
\centering
\end{figure}

\subsection{Usage}

In addition to guaranteeing proper integration with the remaining OpenStack services, one of the main goals of Magnum is to simplify the cluster deployment process and minimize the complexity of user interactions. Listing \ref{MagnumUsage} summarizes all the interaction required for a user to get a running cluster in only a couple minutes. Listing \ref{MagnumScaling} shows how users can easily scale their existing clusters.

\begin{minipage}{\linewidth}
\begin{lstlisting}[language=bash,basicstyle=\footnotesize\ttfamily,caption=Magnum Usage,label=MagnumUsage]
$ magnum cluster-create --name mycluster --cluster-template swarm --node-count 100
$ magnum cluster-list
+------+----------------+------------+--------------+-----------------+
| uuid | name           | node_count | master_count | status          |
+------+----------------+------------+--------------+-----------------+
| .... | mycluster      | 100        | 1            | CREATE_COMPLETE |
+------+----------------+------------+--------------+-----------------+
$ $(magnum cluster-config mycluster --dir magnum/mycluster)
$ docker info / ...
$ docker run --volume-driver cvmfs -v atlas.cern.ch:/cvmfs/atlas -it centos /bin/bash
[root@32f4cf39128d /]#
\end{lstlisting}
\end{minipage}
\begin{minipage}{\linewidth}
\begin{lstlisting}[language=bash,basicstyle=\footnotesize\ttfamily,caption=Magnum Scaling,label=MagnumScaling]
   $ magnum cluster-update mycluster replace node_count=200
   $ magnum cluster-update mycluster replace node_count=5
\end{lstlisting}
\end{minipage}

\subsection{Deployment at CERN} \label{CERN}
\begin{figure}[h]
\label{CERNTimeline}
\caption{Timeline}
\includegraphics[width=1\textwidth]{magnumtimeline}
\end{figure}

Reaching a production container service at CERN involved multiple phases, spread over a year. During the first phase we performed several investigations of the existing container technologies, and tested the possibility of integration of essential CERN services with containers - mainly AFS storage at this point. The first results showed it was possible, although requiring additional privileges that would prevent running containers in a multi-tenant environment - which we worked around later.

The second step, once Magnum appeared as the most viable solution for container integration, involved an initial deployment of this service in the CERN environment. A few additional tasks were required:
\begin{itemize}
\item Deploy Neutron, which we did not have in production at the time. This was done in an isolated Nova cell which allowed us to immediately integrate the service into our production environment without disturbing the rest of the cloud
\item Disable floating IPs, security groups, load balancers and network creation. At CERN we expose to users a single flat network (no tenant networks) using the LinuxBridge plugin (no overlay networks). These were mandatory at the time for Magnum so we run a series of patches locally, which eventually made it upstream later on
\end{itemize}

These changes gave us a working service which we exposed to a set of early pilot users. The initial use cases included continuous integration and builds, infrastructure services (including a prototype for the WLCG File Transfer Service in containers \cite{FTSContainers}), physics analysis using Jupyter/IPython notebooks (Swan\cite{Swan}, MyBinder\cite{MyBinder}) and a few others. The main goal was to identify the missing pieces and work on them before opening the service to all users. We describe the most relevant in Section \ref{StorageIntegration}.

During much of 2016 we worked with our pilot users to debug and adapt the service to their needs, and worked closely with the upstream Magnum team contributing our changes and helping steer the project. The result was a stable service which we opened to all CERN users in October 2016, just a year after the initial investigations.

\subsection{Storage Integration} \label{StorageIntegration}
Storage integration was the top request from all our initial users, which at CERN means CVMFS\cite{CVMFS} and EOS\cite{EOS}.

CVMFS is a read-only distributed filesystem used mainly to scale the distribution of software among the several hundred sites collaborating with CERN experiments. It relies on a large network of cache layers making sure each of the thousands of nodes gets its copy in a timely manner and with minimal load on the network infrastructure. The integration of CVMFS with Magnum at CERN is done via a docker volume plugin \cite{CVMFSPlugin}, which makes sure each repository is made available on the host running the container when requested. If multiple containers in the same host access the same repository they share the mount point and cache layers, optimizing data access. As it's a read-only filesystem, no special user or service credentials are needed and the implementation is much simplified.

EOS handles all physics data coming out of the CERN experiments, already storing hundreds of petabytes and preparing to store even more as luminosity increases and the laboratory prepares itself for a big upgrade in a few years. Data is protected using common POSIX semantics, and access requires an authentication/authorization token, usually Kerberos or X509 certificates. The implementation is again done via a docker volume plugin \cite{EOSPlugin}, but this time significantly more complex due to the need of sharing user certificates in a secure manner for containers running in shared hosts.

In both cases the integration with Kubernetes relies on a wrapper via the FlexVolume plugin \cite{FlexVolume}, which underneath calls the Docker native API so that the same code is reused.

\section{Performance Evaluation} \label{Performance}

Before opening the service to all users, we wanted to answer a series of questions. How fast is cluster deployment? How much concurrent load can Magnum handle? How large can clusters be? How do services deployed in a Magnum cluster scale?

To answer the questions above, we decided to perform a series of benchmark tests against our deployment. For the service benchmarks we used Rally, a tool designed to benchmark OpenStack services which we already used extensively for other services. To benchmark the resources created by Magnum, and evaluate how a service running in a Magnum cluster scales, we used a demo application \cite{KubernetesDemo} published by the Google Cloud Compute team. This demo deploys in a large cluster a web application (running in a replicated http nginx server) serving a static file, and a number of load bots to stress test the service.

\begin{table}[t]
  \begin{varwidth}[b]{0.4\linewidth}
    \centering
    \begin{tabular}{ c c }
      \toprule
      Cluster Size & Time (min) \\
      \midrule
      2 & 2.5 \\
      32 & 4 \\
      128 & 5.5 \\
      512 & 14 \\
      1000 & 23 \\
      \bottomrule
    \end{tabular}
    \caption{Deployment Time}
    \label{RallyBenchmark}
  \end{varwidth}%
  \hfill
  \begin{minipage}[b]{0.55\linewidth}
    \centering
    \includegraphics[width=1.0\linewidth]{kubernetes}
    \captionof{figure}{Kubernetes Scale Test}
    \label{KubernetesScale}
  \end{minipage}
\end{table}
 
The results of the first evaluation are presented in Table \ref{RallyBenchmark}. Numbers for clusters up to 128 nodes are very good, with an almost flat deployment time. Above that there seems to be a close to linear increase following the cluster size, even if 23 minutes is a perfectly acceptable value for deploying a 1000 node cluster. We're working together with the OpenStack Heat team to understand why this linear increase is happening.

Figure \ref{KubernetesScale} shows the result of the Kubernetes scale test, where we reached 7 million requests per second using a 1000 node cluster. This was under the 10 million requests achieved by the Google team, with the issue being understood and pointing to the CERN internal network setup used for this test. We plan to redo the test to confirm this.

\section{Conclusion and Future Work} \label{Conclusion}

In just over a year and starting with a set of early investigations on existing container technologies, the OpenStack Magnum service has been deployed in production at CERN and made available to thousands of users. It allows users to easily deploy Docker Swarm, Kubernetes and Mesos clusters of hundreds of nodes in minutes and integrates seamlessly with the internal identity, resource provisioning and storage services. Our performance evaluation proved it scales to all required use cases, including physics data analysis, high load web services and batch systems.

Additional work will involve improved support for cluster upgrades, which are currently very intrusive on running clusters; node groups, allowing a cluster to be composed of nodes with different flavors, in different AZs or with network access permissions; and cluster healing, with improved recover procedures for broken nodes and services.

We will continue doing this work together with the upstream Magnum development team.

\section*{References}
\bibliographystyle{plain}
\bibliography{chep-container-service.bib}

\end{document}

